<?php

declare(strict_types=1);

namespace AUV_devtool\Exception;

/**
 * 非法的 Authorization 请求头值
 */
class InvalidAuthorizationException extends \Exception
{
}
