<?php

declare(strict_types=1);

namespace AUV_devtool\Exception;

/**
 * 配置不存在.
 */
class ConfigNotFoundException extends \Exception
{
}
