<?php

declare(strict_types=1);

namespace AUV_devtool\Exception;

/**
 * 注解不存在.
 */
class AnnotationNotFoundException extends \Exception
{
}
