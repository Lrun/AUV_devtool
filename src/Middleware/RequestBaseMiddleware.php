<?php


namespace AUV_devtool\Middleware;

use Hyperf\Snowflake\IdGeneratorInterface;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Context\Context;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class RequestIdMiddleware
 * @package AUV_devtool\Middleware
 */
class RequestBaseMiddleware implements MiddlewareInterface
{

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        // 生成请求的RequestId
        $container = ApplicationContext::getContainer();
        $generator = $container->get(IdGeneratorInterface::class);
        $requestId = $generator->generate();
        Context::set('requestId', $requestId);

        // 记录当前时间
        Context::set('requestMillisecondTime', round(microtime(true) * 1000));// 请求进入的毫秒级时间戳
        Context::set('requestSecondTime', time());  // 请求进入的秒级时间戳
        return $handler->handle($request);
    }
}