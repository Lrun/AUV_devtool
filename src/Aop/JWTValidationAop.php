<?php

declare(strict_types=1);

namespace AUV_devtool\Aop;

use AUV_devtool\Annotation\HttpValidation;
use AUV_devtool\Annotation\JWTValidation;
use AUV_devtool\Exception\AnnotationNotFoundException;
use AUV_devtool\Exception\ConfigNotFoundException;
use AUV_devtool\Exception\InvalidAuthorizationException;
use AUV_devtool\Jwt\JWT;
use AUV_devtool\Utils\LazyArrayObject;
use Hyperf\Di\Annotation\Aspect;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Hyperf\HttpServer\Contract\RequestInterface;
use Psr\Container\ContainerInterface;

/**
 * @Aspect(priority=200)
 */
#[Aspect]
class JWTValidationAop extends AbstractAspect
{
    /**
     * @var string[] 要注解的类名称
     */
    public $annotations = [
        JWTValidation::class,
    ];

    /**
     * @Inject
     */
	#[Inject]
    protected JWT $jwt;

    /**
     * @Inject
     */
	#[Inject]
    protected ContainerInterface $container;

    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        /** @var null|JWTValidation $jwtValidation */
        $jwtValidation = $proceedingJoinPoint->getAnnotationMetadata()->method[JWTValidation::class];
        if (!$jwtValidation)
        {
            throw new AnnotationNotFoundException(sprintf('%s::%s() must have @%s annotation', $proceedingJoinPoint->className, $proceedingJoinPoint->methodName, JWTValidation::class));
        }
        $jwt = $this->jwt;
        $config = $jwt->getConfig($jwtValidation->name);
        if (!$config)
        {
            throw new ConfigNotFoundException('Must option the config auv_config.JWT.list');
        }
        $tokenHandler = $config->getTokenHandler();
        if ($tokenHandler)
        {
            $jwtStr = $tokenHandler();
        }
        else
        {
            /** @var RequestInterface $request */
            $request = $this->container->get(RequestInterface::class);
            $authorization = $request->getHeaderLine('Authorization');
            if (!str_contains($authorization, ' '))
            {
                throw new InvalidAuthorizationException('Invalid Authorization');
            }
            [$bearer, $jwtStr] = explode(' ', $authorization, 2);
            if ('Bearer' !== $bearer)
            {
                throw new InvalidAuthorizationException(sprintf('Invalid Authorization value %s', $authorization));
            }
        }
        $token = $jwt->parseToken($jwtStr, $jwtValidation->name ?? $jwt->getDefault(), true);

        // 验证
        if (3 === $jwt->getJwtPackageVersion())
        {
            if ($jwtValidation->tokenParam || $jwtValidation->dataParam)
            {
                $args = $proceedingJoinPoint->arguments;
                if ($jwtValidation->tokenParam)
                {
                    $args['keys'][$jwtValidation->tokenParam] = $token;
                }
                if ($jwtValidation->dataParam)
                {
                    $data = $token->getClaim($config->getDataName());
                    $args['keys'][$jwtValidation->dataParam] = new LazyArrayObject($data);
                }
                $proceedingJoinPoint->arguments = $args;
            }
        }
        else
        {
            if ($jwtValidation->tokenParam || $jwtValidation->dataParam)
            {
                $args = $proceedingJoinPoint->arguments;
                if ($jwtValidation->tokenParam)
                {
                    $args['key'][$jwtValidation->tokenParam] = $token;
                }
                if ($jwtValidation->dataParam)
                {
                    $data = $token->claims()->get($config->getDataName());
                    $args['keys'][$jwtValidation->dataParam] = new LazyArrayObject($data);
                }
                $proceedingJoinPoint->arguments = $args;
            }
        }

        return $proceedingJoinPoint->process();
    }
}
