<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace AUV_devtool\Aop;

use AUV_devtool\Annotation\HttpValidation;
use AUV_devtool\Exception\HttpValidationException;
use AUV_devtool\Utils\LazyArrayObject;
use Hyperf\Di\Annotation\Aspect;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Hyperf\Di\Exception\Exception;
use Hyperf\Validation\Request\FormRequest;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

/**
 * @Aspect
 */
#[Aspect]
class HttpValidationAop extends AbstractAspect
{
    public $annotations = [
        HttpValidation::class,
    ];

    /**
     * @Inject
     */
	#[Inject]
    protected ContainerInterface $container;

    /**
     * @param ProceedingJoinPoint $proceedingJoinPoint
     * @return mixed
     * @throws Exception
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        /** @var null|HttpValidation $annotation */
        $annotation = $proceedingJoinPoint->getAnnotationMetadata()->method[HttpValidation::class];
        if ($annotation) {
            if (! $class = $annotation->class) {
                $_class = explode('\\', $proceedingJoinPoint->className);
                $_count_class = count($_class);
                $controllerName = $_class[$_count_class - 1];
                unset($_class[$_count_class - 1]);
                $class = trim(str_replace('\\Controller', '\\Request', implode('\\', $_class)), '\\') . '\\' . str_replace('Controller', 'Request', $controllerName);
            }
            if (! class_exists($class)) {
                throw new HttpValidationException($class . ' is not defined');
            }
            /** @var null|FormRequest $request */
            $request = $this->container->get($class);
            $scene = $annotation->scene;
            if (! $scene) {
                $scene = $proceedingJoinPoint->methodName;
            }
            $request->scene($scene)->validateResolved();
            if ($annotation->requestParam){
                $requestParam = $request->all();// 拿到请求参数
                $args = $proceedingJoinPoint->arguments;
                if($annotation->safeMode){// 过滤
                    $requestParam = $this->filterParams($requestParam);
                }
                $args['keys'][$annotation->requestParam] = new LazyArrayObject($requestParam);
                $proceedingJoinPoint->arguments = $args;
            }
        }
        return $proceedingJoinPoint->process();
    }


    /**
     * 将每个参数过滤转译
     * @param array $params
     * @return array
     */
    private function filterParams(array $params): array
    {
        // 将每个参数过滤转译
        foreach ($params as $k => $v) {
            if (is_array($v)) {
                $params[$k] = $this->filterParams($v);
            } else {
                $params[$k] = (is_string($v)) ? addslashes(htmlspecialchars($v)) : $v;
            }
        }
        return $params;
    }
}
