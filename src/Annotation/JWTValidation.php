<?php

declare(strict_types=1);

namespace AUV_devtool\Annotation;

use Hyperf\Di\Annotation\AbstractAnnotation;

/**
 * JWT 验证注解.
 *
 * @Annotation
 * @Target("METHOD")
 */
#[\Attribute(\Attribute::TARGET_METHOD)]
class JWTValidation extends AbstractAnnotation
{
    /**
     * @var string|null JWT 配置名称
     */
    public ?string $name = null;

    /**
     * @var string|false|null 验证 ID；为 null 则使用配置中的值验证；为 false 则不验证
     */
    public $id = null;

    /**
     * @var string|false|null 验证发行人；为 null 则使用配置中的值验证；为 false 则不验证
     */
    public $issuer = null;

    /**
     * @var string|false|null 验证接收；为 null 则使用配置中的值验证；为 false 则不验证
     */
    public $audience = null;

    /**
     * @var string|false|null 验证主题；为 null 则使用配置中的值验证；为 false 则不验证
     */
    public $subject = null;

    /**
     * @var string|null Token 对象注入的参数名称
     */
    public ?string $tokenParam = null;

    /**
     * @var string|null 数据注入的参数名称
     */
    public ?string $dataParam = null;

    /**
     * @param mixed ...$value
     */
    public function __construct(...$value)
    {
        parent::__construct(...$value);
        $this->bindMainProperty('name', $value);
    }
}
