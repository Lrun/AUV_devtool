<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace AUV_devtool\Annotation;

use Attribute;
use Hyperf\Di\Annotation\AbstractAnnotation;

/**
 * @Annotation
 * @Target({"METHOD"})
 */
#[Attribute(Attribute::TARGET_METHOD)]
class HttpValidation extends AbstractAnnotation
{
    /**
     * 验证器类名.
     * @var null|string
     */
    public ?string $class = null;

    /**
     * 验证场景.
     * @var null|string
     */
    public ?string $scene = null;


    /**
     * @var string|null 请求参数数据注入的 参数名称
     */
    public ?string $requestParam = null;


    /**
     * @var bool 是否安全模式，安全模式会将参数过滤
     */
    public bool $safeMode = true;

    /**
     * @param mixed ...$value
     */
    public function __construct(...$value)
    {
        parent::__construct(...$value);
        $this->bindMainProperty('class', $value);
    }

}
