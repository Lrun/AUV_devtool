<?php

use Hyperf\Utils\Context;

if (!function_exists('createCo')) {
    /**
     * 创建协程
     * @param $value
     * @return bool|int
     */
    function createCo($value)
    {
        // 获取全局变量
        $requestId = \Hyperf\Utils\Context::get('requestId', '');
        $requestMillisecondTime = \Hyperf\Utils\Context::get('requestMillisecondTime', round(microtime(true) * 1000));
        $requestSecondTime = \Hyperf\Utils\Context::get('requestSecondTime', time());

        return co(function () use ($value, $requestId, $requestMillisecondTime, $requestSecondTime) {
            Context::set('requestId', $requestId);
            Context::set('requestMillisecondTime', round(microtime(true) * 1000));// 请求进入的毫秒级时间戳
            Context::set('requestSecondTime', time());  // 请求进入的秒级时间戳
            $value();
        });
    }
}

if (!function_exists('getMillisecond')) {
    /**
     * 获取毫秒级时间戳
     * @return int
     */
    function getMillisecond(): int
    {
        return (int)(microtime(true) * 1000);
    }
}

if (!function_exists('getFormatDate')) {

    /**
     * 获取格式化后的时间
     * @param int $formatTime
     * @param string $format
     * @return false|string
     */
    function getFormatDate(int $formatTime = 0, string $format = 'Y-m-d H:i:s')
    {
        if ($formatTime == 0) {
            $formatTime = time();
        }
        return date($format, $formatTime);
    }
}


if (!function_exists("objectToarray")) {
    /**
     * 对象转数组-json方式
     * @param Object $object
     * @return mixed
     */
    function objectToArray(object $object)
    {
        return \Hyperf\Utils\Codec\Json::decode(\Hyperf\Utils\Codec\Json::encode($object), true);
    }
}

if (!function_exists("getDistanceByLocation")) {
    /**
     * 计算两个经纬度之间的距离.
     * @param float $longitude1 起点经度
     * @param float $latitude1 起点纬度
     * @param float $longitude2 终点经度
     * @param float $latitude2 终点纬度
     * @param int $unit 单位 1:米 2:公里
     * @param int $decimal 精度 保留小数位数
     */
    function getDistanceByLocation(float $longitude1, float $latitude1, float $longitude2, float $latitude2, int $unit = 2, int $decimal = 2): float
    {
        $earthRadius = 6370.996; // 地球半径系数
        $PI = 3.1415926;
        $radLat1 = $latitude1 * $PI / 180.0;
        $radLat2 = $latitude2 * $PI / 180.0;

        $radLng1 = $longitude1 * $PI / 180.0;
        $radLng2 = $longitude2 * $PI / 180.0;

        $a = $radLat1 - $radLat2;
        $b = $radLng1 - $radLng2;

        $distance = 2 * asin(sqrt(pow(sin($a / 2), 2) + cos($radLat1) * cos($radLat2) * pow(sin($b / 2), 2)));
        $distance = $distance * $earthRadius * 1000;

        if ($unit == 2) {
            $distance = $distance / 1000;
        }
        return round($distance, $decimal);
    }
}

if (!function_exists('generateOrderNo')) {
    /**
     * 生成订单号
     * @param string $prefix 订单前缀
     * @return string
     */
    function generateOrderNo(string $prefix = ''): string
    {
        // 生成请求的RequestId
        $container = \Hyperf\Utils\ApplicationContext::getContainer();
        $generator = $container->get(\Hyperf\Snowflake\IdGeneratorInterface::class);
        $requestId = $generator->generate();
        return $prefix . $requestId;
    }
}