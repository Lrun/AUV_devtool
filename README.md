### auv开发工具
```
                                 **     **     ** **      **   
                                ****   /**    /**/**     /**   
                               **//**  /**    /**/**     /**   
                              **  //** /**    /**//**    **    
                             **********/**    /** //**  **     
                            /**//////**/**    /**  //****      
                            /**     /**//*******    //**       
                            //      //  ///////      //    
```
本工具包提供了：

- 初始化脚本
- 日志类
- 全局异常监听(需配置)
- 数据库语句监听(已通过注解配置)
- 基础请求类

*** 请尽量使用此包进行开发，如果有不能满足的公共功能，请联系作者 ***


##### 脚本操作

    1. php bin/hyperf.php auv:init 创建目录 及删除不必要的文件

##### 手动操作

    在执行完脚本操作成功后操作

    1.将`config/autoload/exceptions.php`中的全局异常处理器`\App\Exception\Handler\FooExceptionHandler::class`替换为`AUV_devtool\Exception\Handler\AppExceptionHandler::class`
    2.在中间件配置文件`config/autoload/middlewares.php`中，将本组件的请求基础中间件`AUV_devtool\Middleware\RequestBaseMiddleware::class` 配置在最顶部

##### 协程上下文相关变量说明

    `requestId` =>  请求的中间件生成的 唯一请求ID
    `requestMillisecondTime`=>请求进入的毫秒级时间戳
    `requestSecondTime` =>请求进入的秒级时间戳

##### 日志类

    日志类提供了多个方法应对不同场景的日志书写

#### HTTP控制器自动验证
    基于hyperf/validation + AOP 的封装
    在控制器的方法上使用@HttpValidation(class="", scenes="")注解，可以自动验证请求参数
    class=验证器类名，默认会自动取Controller同级目录下的Request/xxxRequest.php文件
    scene=场景名称，默认使用控制器同方法名场景

#### 接入 jwt
    借鉴了imiphp jwt接入方式 使用方式完全一样 使用看imiphp文档-> https://doc.imiphp.com/v2.1/components/httpserver/jwt.html